using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    public float speed;
    public Text countText;
    public Text winText;
    private Rigidbody rb;
    private int count;
    public bool isPlayerOne;
    float PlayerOneHorizontal;
    float PlayerOneVertical;
    float PlayerTwoHorizontal;
    float PlayerTwoVertical;
    void Start()
    {
      rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText ();
        winText.text = "";


    }

    void FixedUpdate()
    {
        // Player One will be move by up,down,left,right Keys
        PlayerOneHorizontal = Input.GetAxis("Horizontal");
        PlayerOneVertical = Input.GetAxis("Vertical");

        // Player Two will be move by a,d,w,s Keys
        PlayerTwoHorizontal = Input.GetAxis("PlayerTwoHorizontal");
        PlayerTwoVertical = Input.GetAxis("PlayerTwoVertical");

        if (isPlayerOne)
        {
            rb.AddForce(new Vector3(PlayerOneHorizontal, 0f, PlayerOneVertical) * speed * Time.fixedDeltaTime, ForceMode.Acceleration);
        }
        else
        {
            rb.AddForce(new Vector3(PlayerTwoHorizontal, 0f, PlayerTwoVertical) * speed * Time.fixedDeltaTime, ForceMode.Acceleration);
        }
      
  
    }


    void OnTriggerEnter (Collider other)
        {
            if (other.gameObject.CompareTag("Pick Up"))
            {
                other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText ();
            }
        }  
    
    void SetCountText ()
    {
        countText.text = "Count:" + count.ToString();
        if (count >= 8) 
        {
            winText.text = "You Win !";
        
        }
    }
}
