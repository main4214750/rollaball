using System.Collections;
using System.Collections.Generic;
using UnityEngine;



    public class ButtonClickHandler : MonoBehaviour
    {
        // This function will be called when the button is clicked
        public void OnButtonClick()
        {
            Debug.Log("Button Clicked!");
        }
    }

