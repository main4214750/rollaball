using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityWell : MonoBehaviour


{
    // Strength of the gravitational pull
    public float gravityStrength = 10f;

    // Range within which the gravity affects the player
    public float gravityRange = 5f;

    // Update is called once per frame
    void FixedUpdate()
    {
        // Find all colliders within the gravity range
        Collider[] colliders = Physics.OverlapSphere(transform.position, gravityRange);

        // Apply gravitational pull to nearby objects
        foreach (Collider collider in colliders)
        {
            if (collider.CompareTag("Player"))
            {
                Rigidbody rb = collider.GetComponent<Rigidbody>();
                if (rb != null)
                {
                    Vector3 direction = (transform.position - collider.transform.position).normalized;
                    rb.AddForce(direction * gravityStrength * Time.fixedDeltaTime, ForceMode.Acceleration);
                }
            }
        }
    }
}
